class TableSort {

  /*
   * Constructor
   *
   * @param {Element} table
   * @param {Object} config
   *
   * @return {Void}
  */
  constructor(table, config) {
    this.conf = {
      sortOrder: 'asc',
      cellIndex: 0,
      addSorterEvent: true,
      eventType: 'click',
      tableSearch: false
    };

    Object.assign(this.conf, config);

    if( typeof table === 'string' ){
      table = document.getElementById(table);
    }

    this.table = table;
    this.rows = Array.from(table.querySelectorAll('tbody tr'));
    this.tbody = table.querySelectorAll('tbody');
    this.thds = table.querySelectorAll("th");
    this.hasEventListener = !!document.addEventListener;
    this.handleSortEvent = this.handleSortEvent.bind(this);
    this.handleSearchBarKeyPress = this.handleSearchBarKeyPress.bind(this);
    this.hrs = [];

    if (this.conf.addSorterEvent === false) {
      // Just sort the table on page load
      this.doSort(this.conf.cellIndex, this.conf.sortOrder);
    } else {
      // Otherwise sort the table on event (i.e click)
      for(let i = 0; i < this.thds.length; i++ ) {
        this.addEvent(this.thds[i], this.conf.eventType, this.handleSortEvent);
      }
    };

    if (this.conf.tableSearch) {
      this.searchBar = document.querySelector(`.${this.conf.tableSearch}`);
      this.addEvent(this.searchBar, 'keyup', this.handleSearchBarKeyPress)
    }
  }

  getConfig () {
    return this.conf;
  }

  /*
   * Add an event listener to a dom element.
   * with cross-browser support
   *
   * @param {Element} - element: A DOM Element
   * @param {String} - eventname: name of the event (e.g 'click')
   * @return {Function} - callback: callback function
   *
   * @return {Void}
  */
  addEvent( element, eventName, callback ) {
    if(this.hasEventListener) {
      return element.addEventListener(eventName, callback, false );
    }

    return element.attachEvent( 'on' + eventName, callback);
  };

  /*
   * Handles the table sort
   *
   * @param {Event} event
   *
   * @return {Function} - this.doSort();
  */
  handleSortEvent(event) {
    const activeCell = event.target;
    const cellIndex = activeCell.cellIndex;
    const isSorted = activeCell.getAttribute("data-sort");
    const sortDirection = this.getSortDirection(activeCell);

    for (let i = 0; i < this.thds.length; i++) {
      this.thds[i].removeAttribute('data-sort');
    }
    activeCell.setAttribute('data-sort', sortDirection);

    return this.doSort(cellIndex, sortDirection);
  };


  /*
   * Handles the table search
   *
   * @param {Event} event
   *
   * @return {Function} - this.doSort();
  */
  handleSearchBarKeyPress(event) {
    let serachVal = this.searchBar.value.toLowerCase();
    let trsToShow = this.rows.length;

    this.rows.forEach((row) => {
      let txt = row.innerText.replace(/\s+/g," ").toLowerCase();

      if (txt.indexOf(serachVal) === -1) {
        row.classList.add('hide');
        trsToShow = trsToShow - 1;
      } else {
        row.classList.remove('hide');
      }
    });

    // const noResultsNode = document.createElement('div');
    //
    // if (trsToShow === 0) {
    //   newEl.innerHTML = '<p>No results to show...</p>';
    //   this.table.parentNode.insertBefore(noResultsNode, this.table.nextSibling);
    // } else {
    //   this.table.parentNode.insertBefore(noResultsNode, this.table.nextSibling);
    // }
  }




  /*
   * Get the sort direction
   *
   * @param {Element} activeCell
   *
   * @return {String} - "asc" or "desc"
  */
  getSortDirection (activeCell) {
    const isSorted = activeCell.getAttribute("data-sort");

    return !isSorted || isSorted === 'desc' ? 'asc' : 'desc';
  };

  /*
   * Sorts the table
   *
   * @param {Array} rows
   * @param {Number} cellIndex
   * @param {String} direction
   *
   * @return {Function} - this.updateDom();
  */
  doSort (cellIndex, direction = 'asc') {
    this.rows.sort(function (a, b) {
      const aVal = a.children[cellIndex].innerText.toUpperCase();
      const bVal = b.children[cellIndex].innerText.toUpperCase();

      let isNumber = function () {
        return isNaN(parseInt(aVal)) && isNaN(parseInt(bVal)) ? false : true;
      };

      if (isNumber()) {
        return parseInt(aVal) - parseInt(bVal);
      }

      return new Intl.Collator().compare(aVal, bVal);
    });

    if (direction === 'desc') {
      return this.updateDom(this.rows.reverse());
    }
    return this.updateDom(this.rows, direction);
  };

  /*
   * Update the dom
   *
   * @param {Array} rows
   * @param {String} direction
   *
   * @return {Undefined}
  */
  updateDom (rows, direction) {
    this.tbody[0].innerHTML = '';

    rows.forEach((row) => {
      this.tbody[0].append(row);
    });
  };
}

// new TableSort(document.querySelector('.table-sort'), {});
// new TableSort('js-table-sort', {});

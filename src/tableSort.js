//
// var tableSort = (function () {
//
//   var hasEventListener = !!document.addEventListener;
//
//   addEvent = function( element, eventName, callback ) {
//     if( hasEventListener ) {
//         element.addEventListener(eventName, callback, false );
//     } else {
//         element.attachEvent( 'on' + eventName, callback);
//     }
//   };
//
//   sorterPrototype = {
//     /*
//      * onSortChange
//      *
//      * @param {Event} e
//      *
//      * @return {Function}
//     */
//     onSortChange: function (e) {
//       var that = this;
//       var activeCell = e.target;
//       var rows = Array.from(that.trs);
//       var cellIndex = activeCell.cellIndex;
//       var isSorted = activeCell.getAttribute("data-sort");
//       var sortDirection = !isSorted || isSorted === 'desc' ? 'asc' : 'desc';
//
//       for (var i = 0; i < that.thds.length; i++) {
//         that.thds[i].removeAttribute('data-sort');
//       }
//       activeCell.setAttribute('data-sort', sortDirection);
//
//       return that.sort(rows, cellIndex, sortDirection);
//     },
//
//     /*
//      * Sort
//      *
//      * @param {Array} rows
//      * @param {Object} cellIndex
//      * @param {String} direction
//      *
//      * @return {Void} sorterPrototype
//     */
//     sort: function (rows, cellIndex, direction) {
//       var that = this;
//
//       rows.sort(function (a, b) {
//         var valueA = a.children[cellIndex].innerText.toUpperCase();
//         var valueB = b.children[cellIndex].innerText.toUpperCase();
//
//         var isNumber = function () {
//           return isNaN(parseInt(valueA)) && isNaN(parseInt(valueB)) ? false : true;
//         };
//
//         if (isNumber()) {
//           return parseInt(valueA) - parseInt(valueB);
//         }
//
//         return new Intl.Collator().compare(valueA, valueB);
//       });
//
//       if (direction === 'desc') {
//         return that.updateDom(rows.reverse());
//       }
//       return that.updateDom(rows, direction);
//     },
//
//     /*
//      * @param {Array} rows
//      * @param {String} direction
//      *
//      * @return {Void}
//     */
//     updateDom: function (rows, direction) {
//       var that = this;
//       that.tbody[0].innerHTML = '';
//
//       rows.forEach(function (row) {
//         return that.tbody[0].append(row);
//       });
//     },
//
//
//     /*
//      * @param {Element} <table> element
//      *
//      * @return {Void}
//     */
//     init: function (table) {
//       if( typeof table === 'string' ){
//         table = document.getElementById(table);
//       }
//
//       var that = this;
//       that.table = table;
//       that.tbody = table.querySelectorAll('tbody');
//       that.trs = table.querySelectorAll('tbody tr');
//       that.thds = table.querySelectorAll("th");
//       that.boundSortChange = that.onSortChange.bind(that);
//       that.hrs = [];
//
//       for (var i = 0; i < that.thds.length; i++) {
//         that.hrs.push(that.thds[i]);
//       }
//
//       for( i = 0; i < that.thds.length; i++ ) {
//         addEvent( that.thds[i], 'click', that.boundSortChange);
//       }
//     }
//   }
//
//
//   return {
//     create: function (table) {
//       if (!table || table.tagName !== 'TABLE') {
//         throw new Error('Element must be a table');
//       }
//
//       var sorter = Object.create( sorterPrototype );
//       sorter.init(table);
//       return sorter;
//     }
//   }
// }());
//
//
// var tableId = document.querySelector('.js-table');
// var tableId2 = document.querySelector('.js-table2');
// tableSort.create(tableId);
// tableSort.create(tableId2);
// tableSort.create('table2');
// // tableSort.create('not-a-table');

describe('tableSort', () => {

  describe('init', () => {

    beforeEach(function(){
      fixture.setBase('tests/fixtures');
      result = fixture.load('table-example-1.html');
      table = document.querySelector('.js-table-sort');
      table2 = document.querySelector('.js-table-sort-2');
      tableSort = new TableSort(table);
      customConfig = {
        sortOrder: 'desc',
        cellIndex: 2,
        addSorterEvent: false,
        eventType: 'click'
      }
      tableSortCustom = new TableSort(table2, customConfig);
    });

    afterEach(function(){
      fixture.cleanup();

    });

    it('should initialise with default config values', () => {

      expect(this.tableSort.getConfig()).toEqual({
        sortOrder: 'asc',
        cellIndex: 0,
        addSorterEvent: true,
        eventType: 'click'
      });
    });

    it('should set custom config values when passing an Object', () => {

      // expect(this.tableSortCustom.getConfig()).toEqual(this.customConfig);
    });

    it('should add the event listeners by default', () => {
      // const ts = new TableSort(table2, customConfig)
      spyOn(this.tableSortCustom, 'addEvent');

      expect(this.tableSortCustom.addEvent()).toHaveBeenCalled();
    });
    //
    // it('should sort the table if event listener disabled', () => {
    //   spyOn(this.tableSort, 'doSort');
    //   // this.tableSort.init({
    //   //   sortOrder: 'desc',
    //   //   cellIndex: 2,
    //   //   addSorterEvent: false
    //   // })
    //   expect(this.TableSort.doSort).toHaveBeenCalledWith(2, 'desc');
    // });

  });
});

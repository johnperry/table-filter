'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TableSort = function () {

  /*
   * Constructor
   *
   * @param {Element} table
   * @param {Object} config
   *
   * @return {Void}
  */
  function TableSort(table, config) {
    _classCallCheck(this, TableSort);

    this.conf = {
      sortOrder: 'asc',
      cellIndex: 0,
      addSorterEvent: true,
      eventType: 'click',
      tableSearch: false
    };

    Object.assign(this.conf, config);

    if (typeof table === 'string') {
      table = document.getElementById(table);
    }

    this.table = table;
    this.rows = Array.from(table.querySelectorAll('tbody tr'));
    this.tbody = table.querySelectorAll('tbody');
    this.thds = table.querySelectorAll("th");
    this.hasEventListener = !!document.addEventListener;
    this.handleSortEvent = this.handleSortEvent.bind(this);
    this.handleSearchBarKeyPress = this.handleSearchBarKeyPress.bind(this);
    this.hrs = [];

    if (this.conf.addSorterEvent === false) {
      // Just sort the table on page load
      this.doSort(this.conf.cellIndex, this.conf.sortOrder);
    } else {
      // Otherwise sort the table on event (i.e click)
      for (var i = 0; i < this.thds.length; i++) {
        this.addEvent(this.thds[i], this.conf.eventType, this.handleSortEvent);
      }
    };

    if (this.conf.tableSearch) {
      this.searchBar = document.querySelector('.' + this.conf.tableSearch);
      this.addEvent(this.searchBar, 'keyup', this.handleSearchBarKeyPress);
    }
  }

  _createClass(TableSort, [{
    key: 'getConfig',
    value: function getConfig() {
      return this.conf;
    }

    /*
     * Add an event listener to a dom element.
     * with cross-browser support
     *
     * @param {Element} - element: A DOM Element
     * @param {String} - eventname: name of the event (e.g 'click')
     * @return {Function} - callback: callback function
     *
     * @return {Void}
    */

  }, {
    key: 'addEvent',
    value: function addEvent(element, eventName, callback) {
      if (this.hasEventListener) {
        return element.addEventListener(eventName, callback, false);
      }

      return element.attachEvent('on' + eventName, callback);
    }
  }, {
    key: 'handleSortEvent',


    /*
     * Handles the table sort
     *
     * @param {Event} event
     *
     * @return {Function} - this.doSort();
    */
    value: function handleSortEvent(event) {
      var activeCell = event.target;
      var cellIndex = activeCell.cellIndex;
      var isSorted = activeCell.getAttribute("data-sort");
      var sortDirection = this.getSortDirection(activeCell);

      for (var i = 0; i < this.thds.length; i++) {
        this.thds[i].removeAttribute('data-sort');
      }
      activeCell.setAttribute('data-sort', sortDirection);

      return this.doSort(cellIndex, sortDirection);
    }
  }, {
    key: 'handleSearchBarKeyPress',


    /*
     * Handles the table search
     *
     * @param {Event} event
     *
     * @return {Function} - this.doSort();
    */
    value: function handleSearchBarKeyPress(event) {
      var serachVal = this.searchBar.value.toLowerCase();
      var trsToShow = this.rows.length;

      this.rows.forEach(function (row) {
        var txt = row.innerText.replace(/\s+/g, " ").toLowerCase();

        if (txt.indexOf(serachVal) === -1) {
          row.classList.add('hide');
          trsToShow = trsToShow - 1;
        } else {
          row.classList.remove('hide');
        }
      });

      // const noResultsNode = document.createElement('div');
      //
      // if (trsToShow === 0) {
      //   newEl.innerHTML = '<p>No results to show...</p>';
      //   this.table.parentNode.insertBefore(noResultsNode, this.table.nextSibling);
      // } else {
      //   this.table.parentNode.insertBefore(noResultsNode, this.table.nextSibling);
      // }
    }

    /*
     * Get the sort direction
     *
     * @param {Element} activeCell
     *
     * @return {String} - "asc" or "desc"
    */

  }, {
    key: 'getSortDirection',
    value: function getSortDirection(activeCell) {
      var isSorted = activeCell.getAttribute("data-sort");

      return !isSorted || isSorted === 'desc' ? 'asc' : 'desc';
    }
  }, {
    key: 'doSort',


    /*
     * Sorts the table
     *
     * @param {Array} rows
     * @param {Number} cellIndex
     * @param {String} direction
     *
     * @return {Function} - this.updateDom();
    */
    value: function doSort(cellIndex) {
      var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'asc';

      this.rows.sort(function (a, b) {
        var aVal = a.children[cellIndex].innerText.toUpperCase();
        var bVal = b.children[cellIndex].innerText.toUpperCase();

        var isNumber = function isNumber() {
          return isNaN(parseInt(aVal)) && isNaN(parseInt(bVal)) ? false : true;
        };

        if (isNumber()) {
          return parseInt(aVal) - parseInt(bVal);
        }

        return new Intl.Collator().compare(aVal, bVal);
      });

      if (direction === 'desc') {
        return this.updateDom(this.rows.reverse());
      }
      return this.updateDom(this.rows, direction);
    }
  }, {
    key: 'updateDom',


    /*
     * Update the dom
     *
     * @param {Array} rows
     * @param {String} direction
     *
     * @return {Undefined}
    */
    value: function updateDom(rows, direction) {
      var _this = this;

      this.tbody[0].innerHTML = '';

      rows.forEach(function (row) {
        _this.tbody[0].append(row);
      });
    }
  }]);

  return TableSort;
}();

// new TableSort(document.querySelector('.table-sort'), {});
// new TableSort('js-table-sort', {});
